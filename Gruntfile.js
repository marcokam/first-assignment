module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    coffee:{
      compile:{
        options:{
          bare: true,
          sourceMap: true
        },
        expand: true,
        flatten: false,
        cwd: "src/coffee/",
        src: ["*.coffee"],
        dest: 'build/js/',
        ext: ".js"
      }
    },
    less:{
      compile:{
        options:{
          bare: true,
          sourceMap: true
        },
        expand: true,
        flatten: false,
        cwd: "src/less/",
        src: ["*.less"],
        dest: 'build/css/',
        ext: ".css"
      }
    },
    cssmin: {
       dist: {
          options: {
            keepSpecialComments: 0
          },
          files: [{
            expand: true, // Enable dynamic expansion.
            cwd: 'src/', // Src matches are relative to this path.
            src: ['**/*.css', '!**/_*.css'], // Actual pattern(s) to match. Compile all .scss files except partials
            dest: 'build/', // Destination path prefix.
            ext: '.min.css', // Dest filepaths will have this extension.
        }]
      }
    },
    uglify: {
       dist: {
          files: [{
            expand: true,    // allow dynamic building
            cwd: 'src/', // Src matches are relative to this path.
            src: ['**/*.js', '!**/_*.js'], // Actual pattern(s) to match. Compile all .scss files except partials
            dest: 'build/', // Destination path prefix.
            ext: '.min.js', // Dest filepaths will have this extension.
        }]
       }
    },
    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: 'src/',
          src: ['**/*.{png,jpg,gif,ico}', '!**/_*.{png,jpg,gif,ico}'],
          dest: 'build/',
        }]
      }
    },
    htmlmin: {
      dist: {
        options: {
          removeComments: true,
          collapseWhitespace: true
        },
        files: {
          'index.min.html': 'index.html'
        }
      }
    },
    watch:{
      coffee:{
        files:'src/coffee/*.coffee',
        tasks:'coffee'
      },
      less:{
        files:'src/less/*.less',
        tasks:'less'
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');

  grunt.registerTask('default',
    ['less',
    'coffee',
    'cssmin',
    'uglify',
    'imagemin',
    'htmlmin']);

};