### Project Name ###
- First Assignment

### Project Languages / Plugins ###
- Javascript / node js / coffeescript
- less / css
- html

### Project Installation ###
- download project from bitbucket link (https://cawlanceLansa@bitbucket.org/cawlanceLansa/first-assignment.git)
- then place it anywhere in the "C" drive
- run command inside the directory you must have node js installed for this to work
	* npm install
        * grunt
        * gulp
- after the commands have been run a new folder will be created named "build" and also gulp will now monitor your changes in the src folder and display any error that it may encounter

### Documentation ###
- Grunt (http://gruntjs.com/getting-started)
- Gulp (https://github.com/gulpjs/gulp/blob/master/docs/README.md)