/* File: gulpfile.js */

// grab our gulp packages
var gulp  = require('gulp'),
    jshint = require('gulp-jshint'),
    coffee = require('gulp-coffee'),
    path = require('path'),
    less = require('gulp-less'),
    gutil = require('gulp-util'),
    open = require('gulp-open');

// define the default task and add the watch task to it
gulp.task('default', ['watch','coffee','less','url']);

gulp.task('url', function(){
  gulp.src('index.html')
  .pipe(open());
});

gulp.task('coffee', function() {
  gulp.src('src/coffee/*.coffee')
    .pipe(coffee({bare: true}).on('error', gutil.log))
    .pipe(gulp.dest('build/js'))
});

gulp.task('less', function () {
  return gulp.src('src/less/*.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('build/css'));
});

// configure which files to watch and what tasks to use on file changes
gulp.task('watch', function() {
  gulp.watch('src/coffee/*.coffee', ['coffee']);
  gulp.watch('src/less/*.less', ['less']);
});